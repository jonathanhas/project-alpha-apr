from django.shortcuts import render, redirect
from .forms import TaskForm
from django.contrib.auth.decorators import login_required
from .models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("show_project", id=task.id)

    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/task_create.html", context)


@login_required
def show_my_tasks(request):
    view_tasks = Task.objects.filter(assignee=request.user)
    context = {
        "view_tasks": view_tasks,
        "user": request.user,
    }
    return render(request, "tasks/my_tasks.html", context)
